﻿using System;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Reovo.Lib;
using System.Text.RegularExpressions;

namespace Scoachy.Data
{
    public class DBService: IDbCommand
    {
        private ConnectionStringSettings connectionString;
        private MySqlConnection connection;

        public void Connect()
        {
            Initialise();
        }

        private void Initialise()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MySqlConnectionString"];
            connection = new MySqlConnection(connectionString.ConnectionString);
            connection.Open();
        }

        public MySqlConnection Connection
        {
            get
            {
                return connection;
            }
        }

        public void Disconnect()
        {
            if (connection != null)
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Gets the database configuration from the config file
        /// </summary>
        public void GetConfiguration()
        {
            throw new NotImplementedException();
        }

        public void Reconnect()
        {
            if (connection == null )
            {
                Initialise();
            }
        }

        public bool RunCommand(string parsedSql)
        {
            try
            {
                var cmd = new MySqlCommand(parsedSql, connection);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                LogError(-1, "command", e.Message, connection);
                return false;
            }
            return true;
        }

        public DataTable RunSelect(string parsedSql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (var command = new MySqlCommand(parsedSql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(-1,"select", e.Message, connection);
            }
            return dt;
        }

        public int RunCount(string parsedSql)
        {
            int count = 0;

            try
            {
                using (var command = new MySqlCommand(parsedSql, connection))
                {
                    count = int.Parse(command.ExecuteScalar().ToString());
                }
            }
            catch(Exception e)
            {
                LogError(-1, "count", e.Message, connection);
            }
            return count;
        }

        public void LogError(int code, string origin, string description, MySqlConnection connection)
        {
            description = description.Replace('\'', ' ');
            var loggedErrorSql = string.Format("INSERT INTO `error` (`code`,`origin`,`description`,`errorDate`) VALUES ({0},'{1}','{2}','{3}')",
                code, origin, description, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now));

            MySqlCommand command = new MySqlCommand(loggedErrorSql, connection);
            command.ExecuteNonQuery(); 
        }
    }
}
