﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reovo.Lib
{
    public enum AppSource
    {
        AppleAppStore,
        GooglePlayStore,
        WindowsStore
    }
}
