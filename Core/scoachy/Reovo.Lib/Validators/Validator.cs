﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Globalization;

namespace Reovo.Lib.Validators
{
    public static class Validator
    {
        public static bool Address(string address)
        { return true; }

        public static bool DateOfBirth(string date)
        { return true; }

        public static bool Email(string email)
        {
            var invalid = false;
            if (String.IsNullOrEmpty(email))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper);
            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(email,
                   @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                   @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                   RegexOptions.IgnoreCase);
        }

        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            bool invalid = false;
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }

        public static bool PersonName(string name)
        { return true; }

        public static bool PhoneNumber(string phone)
        { return true; }


        public static bool SQLInjection(string sql)
        {
            bool isSQLInjection = false;

            string[] sqlCheckList = { "--",
                                       ";--",
                                       ";",
                                       "/*",
                                       "*/",
                                        "@@",
                                        "char ",
                                       "nchar ",
                                       "varchar ",
                                       "nvarchar ",                                    
                                       "delete ",
                                       "drop ",
                                       "end",
                                       "exec",
                                       "execute ",
                                       "fetch",
                                       "insert ",
                                       "kill ",
                                       "select ",
                                       "sys",
                                       "sysobjects",
                                       "syscolumns",
                                       " table",
                                       "update ",
                                       " where ",
                                       "=",
                                       " and ",
                                       " or ",
                                       " not "
                                       };

            string CheckString = sql.Replace("'", "''");

            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if ((CheckString.IndexOf(sqlCheckList[i],StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    isSQLInjection = true;
                }
            }

            return isSQLInjection;
        }
    


    }


}
