﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Reovo.Lib
{
    public static class Encryption
    {
        //private static string key = "b89xvC82";

        public static string CustomEncrypt(string plainText, string password)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(plainText);
            Byte[] saltedBytes = Encoding.UTF8.GetBytes(password);

            Byte[] transformedBytes = MixBytes(inputBytes, saltedBytes);
            int[] intArrayVersion = TransformInts(transformedBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < intArrayVersion.Length; i++)
            {
                sb.Append(String.Format("{0:x2}", intArrayVersion[i]));
            }

            return sb.ToString();
        }

        private static int[] TransformInts(byte[] transformedBytes)
        {
            var b = new int[transformedBytes.Length];

            for (int x = 0; x < transformedBytes.Length; x++)
            {
                b[x] = (transformedBytes[x] * 2) + 3;
            }
            return b;
        }

        private static byte[] InvertTransformInts(int[] encryptedBytes)
        {
            var b = new Byte[encryptedBytes.Length];

            for (int x = 0; x < encryptedBytes.Length; x++)
            {
                b[x] = Convert.ToByte((encryptedBytes[x] - 3) / 2);
            }
            return b;
        }

        public static string CustomDecrypt(string encryptedText, string password)
        {
            int[] byteEquivalent = StringToByteArray(encryptedText);

            byte[] original = InvertTransformInts(byteEquivalent);

            var originalBytes = UnmixBytes(original, password.Length);

            return Encoding.UTF8.GetString(originalBytes);
        }

        private static byte[] UnmixBytes(byte[] original, int passwordLength)
        {
            var byteFinal = new List<byte>();

            if (original.Length - passwordLength > passwordLength)
            {
                for (int x = 0; x < original.Length; x++)
                {
                    byteFinal.Add(original[x]);

                    if (x < passwordLength * 2)
                    {
                        x++;
                    }
                }
            }
            else
            {
                for (int x = 0; x < (original.Length - passwordLength) * 2; x += 2)
                {
                    byteFinal.Add(original[x]);
                }
            }
            return byteFinal.ToArray();
        }

        public static int[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToInt32(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string Encrypt(string plainText, string salt)
        {
            HashAlgorithm algorithm = new SHA256CryptoServiceProvider();
            Byte[] inputBytes = Encoding.UTF8.GetBytes(plainText);
            Byte[] saltedBytes = Encoding.UTF8.GetBytes(salt);

            Byte[] transformedBytes = MixBytes(inputBytes, saltedBytes);
            Byte[] hashedBytes = algorithm.ComputeHash(transformedBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
            {
                sb.Append(String.Format("{0:x2}", hashedBytes[i]));
            }

            return sb.ToString();
        }

        private static byte[] MixBytes(byte[] bytes1, byte[] bytes2)
        {
            var bytes1Length = bytes1.Length;
            var bytes2Length = bytes2.Length;
            var totalLength = bytes1Length + bytes2Length;

            byte[] mixedBytes = new byte[totalLength];

            int a = 0;
            int b = 0;
            int x = 0;
            while (x < totalLength)
            {
                if (a < bytes1Length)
                {
                    mixedBytes[x] = bytes1[a];
                    x++;
                    a++;
                }
                if (b < bytes2Length)
                {
                    mixedBytes[x] = bytes2[b];
                    x++;
                    b++;
                }
            }
            return mixedBytes;
        }
    }
}
