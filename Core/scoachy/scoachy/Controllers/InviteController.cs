﻿using Scoachy.Core.Managers;
using Scoachy.Core.Models.Dto;
using System.Web.Http;

namespace scoachy.Controllers
{
    public class InviteController : ApiController
    {
        [Route("invite/send/")]
        [HttpPost]
        public IHttpActionResult PostInvites([FromBody]InviteContactDto contactList)
        {
            var inviteContactManager = new InviteManager();
            var inviteList = inviteContactManager.SendInvites(contactList);
            return Ok(inviteList);
        }

        [Route("invite/list/{userId}")]
        [HttpGet]
        public IHttpActionResult GetUpdatedInviteList(string userId)
        {
            var inviteContactManager = new InviteManager();
            var inviteList = inviteContactManager.GetInviteList(userId);
            return Ok(inviteList);
        }

        [Route("invite/changestatus/")]
        [HttpPost]
        public IHttpActionResult ChangeStatus([FromBody] InviteStatusDto inviteStatus )
        {
            var inviteContactManager = new InviteManager();
            var changeStatus = inviteContactManager.ChangeStatus(inviteStatus);
            return Ok(changeStatus);
        }

        [Route("invite/friendlist/{userId}")]
        [HttpGet]
        public IHttpActionResult GetUpdatedFriendList(string userId)
        {
            var inviteContactManager = new InviteManager();
            var inviteList = inviteContactManager.GetFriendList(userId);
            return Ok(inviteList);
        }

        [Route("invite/resend/{userId}")]
        public IHttpActionResult GetResendInvitesForUser(string userId)
        {
            var inviteContactManager = new InviteManager();
            var result = inviteContactManager.GetResendInvites(userId);
            return Ok(result);
        }
    }
}
