﻿using Scoachy.Core.Managers;
using Scoachy.Core.Models.Dto;
using System.Collections.Generic;
using System.Web.Http;

namespace scoachy.Controllers
{
    public class ResultsController : ApiController
    {
        [Route("results/send/")]
        [HttpPost]
        public IHttpActionResult SendResults([FromBody]ShareMultipleResultsDto userResults)
        {
            var resultsManager = new ResultsManager();
            var status = resultsManager.Send(userResults);
            return Ok(status);
        }

        [Route("results/sendflat/")]
        [HttpPost]
        public IHttpActionResult SendResults([FromBody]List<FlatResultsDto> userResults)
        {
            var resultsManager = new ResultsManager();
            var status = resultsManager.SendFlat(userResults);
            return Ok(status);
        }

        [Route("results/summary/{userId}")]
        [HttpGet]
        public IHttpActionResult GetResultsSummaryForUser(string userId)
        {
            var resultsManager = new ResultsManager();
            var results = resultsManager.GetSummary(userId);
            return Ok(results);
        }
    }
}