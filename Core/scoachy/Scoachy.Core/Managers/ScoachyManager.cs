﻿using Newtonsoft.Json;
using Reovo.Lib;
using Scoachy.Core.Managers.Interfaces;
using Scoachy.Core.Models;
using Scoachy.Core.Models.Dto;
using Scoachy.Core.Models.Invite;
using Scoachy.Core.Models.User;
using Scoachy.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Scoachy.Core.Models.Scoachy;
using MySql.Data.MySqlClient;

namespace Scoachy.Core.Managers
{
    public class ScoachyManager : IScoachyManager
    {
        private DBService dbService;

        public ScoachyManager()
        {
            dbService = new DBService();
        }

        public decimal getLowerAppVersion()
        {
            return 1.01m;
        }

        public decimal getHigherAppVersion()
        {
            return 2.0m;
        }

        public int sqlcommandUser(UserStatus status)
        {
            var command=string.Format("SELECT COUNT(*) FROM user WHERE userStatusId='" + EnumUtils.GetEnumDescription(status) + "';");
            return dbService.RunCount(command);
        }

        public int sqlcommandInvite(InviteStatus status)
        {
            var command = string.Format("SELECT COUNT(*) FROM invite WHERE inviteStatusId='" + EnumUtils.GetEnumDescription(status) + "';");
            return dbService.RunCount(command);
        }

        public int TotalNumUsers()
        {
            var command = string.Format("SELECT COUNT(*) FROM user;");
            return dbService.RunCount(command);
        }

        public int TotalNumInvites()
        {
            var command = string.Format("SELECT COUNT(*) FROM invite;");
            return dbService.RunCount(command);
        }

        public int TotalActiveConnections()
        {
            var command = string.Format("SELECT COUNT(*) FROM INFORMATION_SCHEMA.PROCESSLIST;");
            return dbService.RunCount(command);
        }

        public Stats GetStats()
        {
            String connection;
            if (ServerConnection() == true)
            {
                connection = "Alive: Yes";
            }
            else
            {
                connection = "Alive: No";
            }
            dbService.Connect();
            int numActiveUser = sqlcommandUser(UserStatus.Active);
            int numInactiveUser = sqlcommandUser(UserStatus.Inactive);
            int numAwaitingConfirmationUser = sqlcommandUser(UserStatus.AwaitingConfirmation);
            int numForgotPasswordUser = sqlcommandUser(UserStatus.ForgotPassword);
            int numTotalUser = TotalNumUsers();
            int numAcceptedInvites = sqlcommandInvite(InviteStatus.Accepted);
            int numPendingInvites = sqlcommandInvite(InviteStatus.Pending);
            int numTotalInvites = TotalNumInvites();
            int numActiveConnections = TotalActiveConnections();
            dbService.Disconnect();
            string version = GetBackendVersion();
            Stats stats = new Stats {
                DatabaseConnectionStatus = connection,
                ActiveUsers = numActiveUser,
                InactiveUsers = numInactiveUser,
                AwaitingConfirmationUsers = numAwaitingConfirmationUser,
                ForgotPasswordUsers = numForgotPasswordUser,
                TotalUsers = numTotalUser,
                AcceptedInvites = numAcceptedInvites,
                PendingInvites = numPendingInvites,
                TotalInvites = numTotalInvites,
                ActiveConnections = numActiveConnections,
                paths =null, BackendVersionNumber = version };
            //string JSONResponse = JsonConvert.SerializeObject(stats);
            return stats;
        }

        private string GetBackendVersion()
        {
            //Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            //DateTime buildDate = new DateTime(2018, 1, 1)
            //                        .AddDays(version.Build).AddSeconds(version.Revision * 2);
            //string displayableVersion = $"{version} ({buildDate})";

            string displayableVersion = "1.0." + "20180405.02";
            return displayableVersion;
        }

        public bool ServerConnection()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["MySqlConnectionString"];
            MySqlConnection connection = new MySqlConnection(connectionString.ConnectionString);
            using (connection)
            {
                try
                {
                    connection.Open();
                    connection.Close();
                    return true;
                }
                catch (SqlException)
                {
                    connection.Dispose();
                    return false;
                }
               
            }
        }

        public StatusDto SendVersion(decimal appVersion)
        {
            if (appVersion < getLowerAppVersion() ||
                appVersion > getHigherAppVersion())
            {
                return new StatusDto
                {
                    ErrorCode = (int)StatusCode.ScoachyVersionOutdated,
                    Message = "Scoachy version is outdated",
                    Success = false
                };
            }

            return new StatusDto
            {
                ErrorCode = 0,
                Message = string.Empty,
                Success = true
            };
        }

        public bool ConfirmPayment(ConfirmPaymentDto paymentConfirmation)
        {
            dbService.Connect();
            try
            {
                // Update existing user in user 
                var updateUser = string.Format(
                    "UPDATE `user` SET userTypeId = '{0}' " +
                    "WHERE Id = '{1}'",
                    EnumUtils.GetEnumDescription(UserType.Paid), paymentConfirmation.UserId);

                dbService.RunCommand(updateUser);

                var insertPaymentInfo = string.Format(
                    "INSERT INTO usertransaction (`id`,`userId`,`appSource`,`transactionDate`,`storeTransactionId`,`orderId`) " +
                    "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')",
                    Guid.NewGuid().ToString(), paymentConfirmation.UserId, paymentConfirmation.ApplicationSource,
                    paymentConfirmation.PaymentDate, paymentConfirmation.StoreTransactionId, paymentConfirmation.OrderId
                    );

                dbService.RunCommand(insertPaymentInfo);
            }
            catch (Exception e )
            {
                dbService.LogError(-1, "confirmpayment", e.Message, dbService.Connection);
            }
            finally
            {
                dbService.Disconnect();
            }
            return true;
        }

        public HomeScreenStatsDto GetGlobalStatistics()
        {
            dbService.Connect();

            try
            { 
                var globalStatsSql = string.Format("SELECT count(id) FROM `user` WHERE userStatusId = '{0}'",
                    EnumUtils.GetEnumDescription(UserStatus.Active)
                    );
                var scoached = dbService.RunCount(globalStatsSql);

                var userRegisteredSql = string.Format("SELECT count(id) FROM `invite` WHERE inviteStatusId = '{0}' GROUP BY friendUserId",
                    EnumUtils.GetEnumDescription(InviteStatus.Accepted)
                    );

                var scoachers = dbService.RunCount(userRegisteredSql);

                dbService.Disconnect();

                var scoachyStats = new HomeScreenStatsDto
                {
                    Scoachers = scoachers,
                    Scoached = scoached
                };
                return scoachyStats;
            }
            catch (Exception e)
            {
                dbService.LogError(-1, "globalstats", e.Message, dbService.Connection);
            }
            finally
            {
                dbService.Disconnect();
            }

            return new HomeScreenStatsDto
            {
                Scoachers = 0,
                Scoached = 0
            };
        }

        public SyncDataDto GetSyncData(string userId, int dbVersion)
        {
            var syncData = new SyncDataDto();
            try
            {
                var results = new ResultsManager(dbService);
                var account = new AccountManager(dbService);
                syncData.Status = new StatusDto
                {
                    ErrorCode = 0,
                    Message = string.Empty,
                    Success = true
                };

                if (dbVersion < 1)
                {
                    syncData.Status = new StatusDto
                    {
                        ErrorCode = 1,
                        Message = "Version not correct",
                        Success = false
                    };
                }
                dbService.Connect();
                syncData.UserInfo = account.GetUser(userId);
                dbService.Disconnect();
                syncData.AnswerResults = results.GetSummary(userId);
                dbService.Connect();
                syncData.EncryptedSql = FetchSql(dbVersion);
                syncData.LastDbVersion = GetMaxDbUpdateVersion();
            }
            catch (Exception e)
            {
                dbService.LogError(-1, "syncservice", e.Message, dbService.Connection);
            }
            finally {
                dbService.Disconnect();
            }
            return syncData;
        }

        private string FetchSql(int dbVersion)
        {
            var selectDbUpdatesSql = string.Format("SELECT sqltext FROM `dbupdates` WHERE version > {0} ORDER BY version", dbVersion);

            var sqlUpdatesList = dbService.RunSelect(selectDbUpdatesSql);
            IList<string> sqlList = sqlUpdatesList.AsEnumerable().Select(row =>
                    row.Field<string>("sqltext")
            ).ToList();

            var sqlString = String.Join(";", sqlList);

            return sqlString;
        }

        private int GetMaxDbUpdateVersion()
        {
            int lastVersion;
            try
            { 
                var selectMaxSql = string.Format("SELECT MAX(version) as lastVersion FROM `dbupdates`");

                var sqlVersionsList = dbService.RunSelect(selectMaxSql);
                IList<int> versionList = sqlVersionsList.AsEnumerable().Select(row =>
                        row.Field<int>("lastVersion")
                ).ToList();

                if (versionList == null) return 0;

                lastVersion = versionList.FirstOrDefault();
            }
            catch(Exception)
            {
                return 1;
            }
            return lastVersion;
        }

        public IList<Setting> GetSettings()
        {
            var settingsSql = string.Format("SELECT id, name, description, settingsTypeId, value FROM settings");

            dbService.Connect();
            var settingsTable = dbService.RunSelect(settingsSql);
            dbService.Disconnect();

            IList<Setting> settings = settingsTable.AsEnumerable().Select(row => 
                new Setting
                {
                    Id = row.Field<string>("id"),
                    Name = row.Field<string>("name"),
                    Description = row.Field<string>("description"),
                    SettingsType = row.Field<string>("settingsTypeId"),
                    Value = row.Field<string>("value")
                }
            ).ToList();

            return settings;
        }

        public bool ReportIssue(ReportIssueDto issue)
        {
            var insertReportSql = string.Format("INSERT INTO `userreport` (`id`,`userId`,`name`,`category`,`description`,`issueDate`) " + 
                "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')",
                Guid.NewGuid().ToString(), issue.UserId, issue.Name, issue.Category, 
                issue.Description, ConversionUtils.ConvertDateToMySqlFormat(DateTime.Now));

            dbService.Connect();
            var result = dbService.RunCommand(insertReportSql);
            dbService.Disconnect();
            return result;
        }

        public void GetDashboardInfo()
        {
            return;
        }
    }
}
