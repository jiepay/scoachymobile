﻿using Scoachy.Core.Models.Dto;
using Scoachy.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers.Interfaces
{
    public interface IAccountManager
    {
        RegisterUserReturnDto RegisterUser(RegisterUserDto user);
        UserLoginReturnDto UserLogin(UserLoginDto user);
        UserLoginReturnDto ForgetPassword(UserLoginDto user);
        RegisterUserReturnDto UpdateUser(RegisterUserDto user);
    }
}
