﻿using Scoachy.Core.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers.Interfaces
{
    public interface IInviteManager
    {
        InviteContactDto SendInvites(InviteContactDto contactList);
        InviteContactDto GetInviteList(string userId);
        InviteContactDto GetFriendList(string userId);
    }
}
