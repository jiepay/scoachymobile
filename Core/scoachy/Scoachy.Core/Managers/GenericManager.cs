﻿using Reovo.Lib;
using Scoachy.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Managers
{
    public class GenericManager
    {
        public static string GetLanguageCode(string language)
        {
            int code = (int)Language.French;

            switch (language.ToLower())
            {
                case "english":
                    code = (int)Language.English;
                    break;
                case "french":
                    code = (int)Language.French;
                    break;
                default:
                    code = (int)Language.French;
                    break;
            }

            return code.ToString();
        }

        public static string ConvertToUserType(string userType)
        {
            userType = userType.ToLower();

            if (userType == "free")
            {
                return EnumUtils.GetEnumDescription(UserType.Free);
            }
            else if (userType == "paid")
            {
                return EnumUtils.GetEnumDescription(UserType.Paid);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string ConvertToMediaType(string registrationMode)
        {
            string mediaTypeId = string.Empty;

            switch (registrationMode.ToLower())
            {
                case "email":
                    mediaTypeId = EnumUtils.GetEnumDescription(MediaType.Email);
                    break;
                case "google":
                    mediaTypeId = EnumUtils.GetEnumDescription(MediaType.Google);
                    break;
                case "facebook":
                    mediaTypeId = EnumUtils.GetEnumDescription(MediaType.Facebook);
                    break;
                case "linkedin":
                    mediaTypeId = EnumUtils.GetEnumDescription(MediaType.LinkedIn);
                    break;
                default:
                    mediaTypeId = string.Empty;
                    break;
            }
            return mediaTypeId;
        }
    }
}
