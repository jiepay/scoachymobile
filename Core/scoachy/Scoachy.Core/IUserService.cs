﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core
{
    interface IUserService
    {
        void createAccount();
        void lockAccount();
        void forgotPassword();
        void buyProVersion();
        void logOut();
        void inviteUser(string userName);
    }
}
