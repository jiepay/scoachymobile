﻿using Newtonsoft.Json;
using Scoachy.Core.Models.User;
using System;

namespace Scoachy.Core.Models.Dto
{
    public class RegisterUserDto: RootDto
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; } //default is null

        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "Gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "BirthDate")]
        public string BirthDate { get; set; } //yyyy-mm-dd

        [JsonProperty(PropertyName = "PostalCode")]
        public string PostalCode { get; set;  }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "PhonePrefix")]
        public string PhonePrefix { get; set;  }

        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "EncryptedPassword")]
        public string EncryptedPassword { get; set; } //min.8, max 32.characters

        [JsonProperty(PropertyName = "Salt")]
        public string Salt { get; set;  }

        [JsonProperty(PropertyName = "Language")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "ReceiveNewsletter")]
        public bool ReceiveNewsletter { get; set; }

        [JsonProperty(PropertyName = "RegistrationMode")]
        public string RegistrationMode { get; set;  } //email/google/facebook

        [JsonProperty(PropertyName = "DeviceToken")]
        public string DeviceToken { get; set; } //guid: 2f3e50cc-9df7-11e7-82e6-000c29a56235

        [JsonProperty(PropertyName = "Os")]
        public string Os { get; set; }

        [JsonProperty(PropertyName = "OsVersion")]
        public string OsVersion { get; set; }

        [JsonProperty(PropertyName = "UserStatus")]
        public string UserStatus { get; set; } //default is null

        //TransactionId missing from playstore/appstore
        [JsonProperty(PropertyName = "UserType")]
        public string UserType { get; set;  } //free or paid

        public RegisterUserDto()
        {

        }
    }

    public class RegisterUserReturnDto
    {
        public string Id { get; set; }
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }

        public RegisterUserReturnDto()
        {
            Success = true;
            ErrorCode = 0;
            Message = string.Empty;
        }
    }

    public class UserLoginDto: RootDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public bool ForgetPassword { get; set; } //default is false, true for forget password
        public string UserStatus { get; set; }
        public string DeviceToken { get; set; } //guid: 2f3e50cc-9df7-11e7-82e6-000c29a56235
        public string Os { get; set; }
        public string OsVersion { get; set; }
    }

    public class ForgetPasswordDto
    {
        public string UserId { get; set; }
        public string RandomPassword { get; set; }
        public DateTime ExpiryDate { get; set; }
        public bool Serviced { get; set; }
    }

    public class UserLoginReturnDto
    {
        public string Id { get; set; }
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }

        public UserLoginReturnDto()
        {
            Success = true;
            ErrorCode = 0;
            Message = string.Empty;
        }
    }
}