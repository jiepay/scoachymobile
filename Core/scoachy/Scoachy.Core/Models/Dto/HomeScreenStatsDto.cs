﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scoachy.Core.Models.Dto
{
    public class HomeScreenStatsDto
    {
        public int Scoachers { get; set; }
        public int Scoached { get; set; }
    }
}