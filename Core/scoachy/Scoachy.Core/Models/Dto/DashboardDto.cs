﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Dto
{
    public class DashboardDto
    {
        public string ConnectionStatus { get; set; }
        public List<string> ServiceList { get; set; }
        public int NumberOfUsers { get; set; }
        public int NumberOfInvites { get; set; }
        public string ReplicationServiceStatus { get; set; }
    }
}
