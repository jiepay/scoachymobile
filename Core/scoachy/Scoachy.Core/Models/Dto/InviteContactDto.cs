﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace Scoachy.Core.Models.Dto
{
    public class InviteContactDto
    {
        [JsonProperty(PropertyName ="UserId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "ContactsDto")]
        public IList<ContactDto> ContactsDto { get; set; }

        [JsonProperty(PropertyName = "ErrorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty(PropertyName = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }

    public class ContactDto
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set;  }

        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "FriendUserId")]
        public string FriendUserId { get; set;  }

        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set;  }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; } // either email OR phone mandatory

        [JsonProperty(PropertyName = "PhonePrefix")]
        public string PhonePrefix { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "InviteStatus")]
        public string InviteStatus { get; set; }

        [JsonProperty(PropertyName = "UserStatus")]
        public string UserStatus { get; set; }

        [JsonProperty(PropertyName = "InvitedOn")]
        public string InvitedOn { get; set; }

        [JsonProperty(PropertyName = "AcceptedOn")]
        public string AcceptedOn { get; set; }

        public bool MarkToDelete { get; set; }

        [JsonProperty(PropertyName = "ErrorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty(PropertyName = "ErrorMessage")]
        public string ErrorMessage { get; set; }
    }

    public class InviteStatusDto
    {
        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set;  }

        [JsonProperty(PropertyName = "FriendUserId")]
        public string FriendUserId { get; set; }
    }
}
