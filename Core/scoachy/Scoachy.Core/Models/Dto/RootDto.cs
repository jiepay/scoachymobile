﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scoachy.Core.Models.Dto
{
    public class RootDto
    {
        public string RootUserId { get; set; }
        public decimal AppVersion { get; set; }
    }
}
