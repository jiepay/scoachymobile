﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Scoachy.Core.Models.Dto
{
    public class InvitesDto
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set;  }

        [JsonProperty(PropertyName = "UserdId")]
        public string UserdId { get; set; }

        [JsonProperty(PropertyName = "InvitedOn")]
        public string InvitedOn { get; set; }

        [JsonProperty(PropertyName = "AcceptedOn")]
        public string AcceptedOn { get; set; }

        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "PhonePrefix")]
        public string PhonePrefix { get; set; }

        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "InviteStatusId")]
        public string InviteStatusId { get; set; }

        [JsonProperty(PropertyName = "MediaTypeId")]
        public string MediaTypeId { get; set; }
    }
}
