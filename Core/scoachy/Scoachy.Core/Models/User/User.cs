﻿using Newtonsoft.Json;
using System;

namespace Scoachy.Core.Models.User
{
    public class User
    {
        public string Id { get; set; }
        public string UserNumber { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public string CreatedOn { get; set; }
        public string LastLogin { get; set; }
        public string UserTypeId { get; set; }
        public int LoginAttempt { get; set; }
        public string LanguageId{ get; set; }
        public string UserStatusId { get; set; }
        public string MediaType { get; set; }
        public string DeviceToken { get; set; }
        public string Os { get; set; }
        public string OsVersion { get; set; }

        public User()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public User(string name)
        {
            UserName = name;
        }
    }
}
