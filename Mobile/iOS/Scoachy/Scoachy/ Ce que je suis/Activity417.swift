//
//  Activity417.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 26/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity417: UIViewController {
    
    @IBOutlet weak var lblMission: UILabel!
    @IBOutlet weak var txtMission: UITextField!
    
    var talent1 = "talent1"
    var talent2 = "talent2"
    var talent3 = "talent3"
    var talent4 = "talent4"
    var talent5 = "talent5"
    
    var valeur1 = "valeur1"
    var valeur2 = "valeur2"
    var valeur3 = "valeur3"
    var valeur4 = "valeur4"
    var valeur5 = "valeur5"
    
    var attribute1 = "attribut1"
    var attribute2 = "attribut2"
    var attribute3 = "attribut3"
    var attribute4 = "attribut4"
    var attribute5 = "attribut5"
    
    var verbeMoteur = "verbe moteur"
    var publicCible = "public cible"
    var txtSaisie = ""
    
    var verbSelected = [String]()
    var valuesSelected = [String]()
    var attributeSelected = [String]()
    var moteurArray = [String]()
    var cibleArray = [String]()

    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        var userDefaults = UserDefaults.standard
        userDefaults.set(txtMission.text, forKey: "txtSaisie")
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            for row in try db.prepare("SELECT talentverb.verb FROM talentverb INNER JOIN talentselected ON talentverb.id = talentselected.verbId WHERE talentselected.userId = '\((usersprofileid)!)'"){
                
                let talentverb = row[0] as! String
                verbSelected.append(talentverb)
                
            }
            
            for row in try db.prepare("SELECT myvaluesdescription FROM myvaluesselected where userId = '\((usersprofileid)!)'"){
                
                let valSelected = row[0] as! String
                valuesSelected.append(valSelected)
                
            }
            
            for row in try db.prepare("select r.answerId as answerId ,r.questionId as questionId, count(r.answerId) as cntresult, o.ratingtypeId as ratingtypeId , o.description as description FROM answerresult r, allquestion o WHERE r.questionId = o.id AND r.userid = '\((usersprofileid)!)' AND o.ratingtypeId = '529510c6-233d-11e7-b36e-c03fd56ee88c' AND o.attributeId = '24551cf0-5cb4-11e7-a11e-7a791928939c' GROUP BY r.answerId order by cntresult limit 5"){
                
                let attribute = row[4] as! String
                attributeSelected.append(attribute)
            }
            
            for row in try db.prepare("select answer from missionanswer where missionid = 'ba311fa6-3bc4-11e8-b467-0ed5f89f718b' AND userid = '\((usersprofileid)!)'"){
                
                let moteur = row[0] as! String
                moteurArray.append(moteur)
                
            }
            
            for row in try db.prepare("select answer from missionanswer where missionid = 'ba3122e4-3bc4-11e8-b467-0ed5f89f718b' AND userid = '\((usersprofileid)!)'"){
                
                let cible = row[0] as! String
                cibleArray.append(cible)
                
            }
            
            
            if !(verbSelected.isEmpty) {
                talent1 = verbSelected[0]
                talent2 = verbSelected[1]
                talent3 = verbSelected[2]
                talent4 = verbSelected[3]
                talent5 = verbSelected[4]
            }
            
            if !(valuesSelected.isEmpty) {
                valeur1 = valuesSelected[0]
                valeur2 = valuesSelected[1]
                valeur3 = valuesSelected[2]
                valeur4 = valuesSelected[3]
                valeur5 = valuesSelected[4]
            }
            
            if !(attributeSelected.isEmpty) {
                attribute1 = attributeSelected[0]
                attribute2 = attributeSelected[1]
                attribute3 = attributeSelected[2]
                attribute4 = attributeSelected[3]
                attribute5 = attributeSelected[4]
            }
            
            if !(moteurArray.isEmpty){
                verbeMoteur = moteurArray[0]
            }
            
            if !(cibleArray.isEmpty){
                publicCible = cibleArray[0]
            }
            
            print("test")
            print(moteurArray)
            print(cibleArray)
            
            var userDefaults = UserDefaults.standard
            var txtSaisies = UserDefaults.standard.string(forKey: "txtSaisie")
            
            if !(txtSaisies == nil) {
                txtSaisie = txtSaisies!
            }
//            voici le texte à mettre  et si possible dans la même forme c'est-à-dire d'aller à la ligne stp!
//            Avec mes talents (ici apparaissent les 5 talents sélectionnés ) vos 5 talents
//            et mes qualités ( ici apparaissent les 5 attributs positifs sélectionnés) vos 5 attributs
//            tout en respectant mes valeurs (ici apparaissent les 5 valeurs sélectionnées) vos 5 valeurs
//            (ici apparaît le "verbe moteur" qui en fait peut être une phrase) votre "verbe moteur"
//            (ici apparaît un public cible sélectionné) votre public cible
//            afin d'obtenir plus de (ici c'est une saisie libre ) écrivez ici ce qui est essentiel à vos yeux. Exemple: le bonheur, la liberté, l'amour, le plaisir etc.
            
            lblMission.text = "Avec mes talents: \(talent1), \(talent2), \(talent3), \(talent4), \(talent5) vos 5 talents. \net mes qualités: \(attribute1), \(attribute2), \(attribute3), \(attribute4), \(attribute5) vos 5 attributs \nTout en respectant des valeurs essentielles à mes yeux: \(valeur1), \(valeur2), \(valeur3), \(valeur4), \(valeur5) vos 5 valeurs. \n\(verbeMoteur) votre 'verbe moteur' \n\(publicCible) votre public cible \nafin d'obtenir plus de: \(txtSaisie)"
            
        }
            
        catch {
            print("Connection to database failed")
            
        }

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
