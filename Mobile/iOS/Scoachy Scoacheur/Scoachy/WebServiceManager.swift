//
//  WebServiceManager.swift
//  Scoachy
//
//  Created by Jean Paul on 29/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation

class WebServiceManager
{
    let scoachybackend_com = "192.168.6.57"
    let portNo = "8088"
    
    public static func postWebServiceBuilder(serviceCallAddress: String, jsonData:[String:Any])->[String:Any]
    {
        guard let url = UrlBuilder(serviceCallAddress: serviceCallAddress) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let parameters: [String: Any] = jsonData
        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            request.httpBody = jsonBody
        } catch {}
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, _, _) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {}
        }
        task.resume()
        return true
    }
    
    public static func UrlBuilder(serviceCallAddress: String)->URL?
    {
        return URL(string: "http://" + scoachybackend_com + ":" + portNo + "/" + serviceCallAddress)
    }
}
