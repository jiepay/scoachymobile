//
//  Activity43.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 14/12/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import UIKit
import SQLite

class Activity43: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrayDate = [String]()
    var arrayPercentage = [String]()
    
    var userDefaults = UserDefaults.standard
    var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return arrayDate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LaRoueDeLaVie
        
        cell.date.text = arrayDate[indexPath.row]
        cell.progress.text = String(arrayPercentage[indexPath.row])
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var userDefaults = UserDefaults.standard
        var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")
        
        print("indexpath: \(indexPath.row)")
        print(self.arrayPercentage[indexPath.row])

        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, index) in
            
           
            
            let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
            print(bundlePath ?? "", "\n") //prints the correct path
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let fileManager = FileManager.default
            let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
            let fullDestPathString = fullDestPath!.path
            print(fileManager.fileExists(atPath: bundlePath!)) // prints true
            if fileManager.fileExists(atPath: fullDestPathString) {
                print("File is available")
            }else{
                do{
                    try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
                }catch{
                    print("\n")
                    print(error)
                    
                }
            }
            
            do {
                let db = try Connection(fullDestPathString, readonly:false)
                let stmt = try db.run("DELETE FROM wheelhistory WHERE userid= '\((usersprofileid)!)' and percentagevalue = '\(self.arrayPercentage[indexPath.row])';")
                self.arrayDate.remove(at: indexPath.row)
                self.arrayPercentage.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }

            catch {
                print(error)
            }
        }
        
        return [deleteAction]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let wheelhistory = Table("wheelhistory")
        let wid = Expression<String>("id")
        let wuserid = Expression<String>("userid")
        let wdate = Expression<Date>("date")
        let wpercentagevalue = Expression<Int>("percentagevalue")
        
        let bundlePath = Bundle.main.path(forResource: "scoachy", ofType: ".sqlite3")
        print(bundlePath ?? "", "\n") //prints the correct path
        let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let fileManager = FileManager.default
        let fullDestPath = NSURL(fileURLWithPath: destPath).appendingPathComponent("scoachy.sqlite3")
        let fullDestPathString = fullDestPath!.path
        print(fileManager.fileExists(atPath: bundlePath!)) // prints true
        if fileManager.fileExists(atPath: fullDestPathString) {
            print("File is available")
        }else{
            do{
                try fileManager.copyItem(atPath: bundlePath!, toPath: fullDestPathString)
            }catch{
                print("\n")
                print(error)
                
            }
        }
        
        do {
            let db = try Connection(fullDestPathString, readonly:false)
            
            var userDefaults = UserDefaults.standard
            //var usersprofileid = UserDefaults.standard.string(forKey: "userprofileid")!
            
            let stmt = try db.prepare("SELECT * FROM wheelhistory where userid = '\((usersprofileid)!)'")
            
            for row1 in stmt {
                
                let date = row1[2] as! String
                let percentage = row1[3] as! String
                
                arrayDate.append(date)
                arrayPercentage.append(percentage)
                
            }
            
            
            print(arrayDate)
            print(arrayPercentage)
            self.tableView.reloadData()
        }
            
        catch {
            print(error)
            
        }
        
        
        
    }
    
    @IBAction func closeButton(_sender: Any) {
        
        let segmentIndex = UserDefaults.standard
        segmentIndex.set(3, forKey: "segmentIndex")
        _ = navigationController?.popToRootViewController(animated: false)
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
