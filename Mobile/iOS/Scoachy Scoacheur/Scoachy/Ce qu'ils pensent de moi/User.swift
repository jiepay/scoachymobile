//
//  User.swift
//  Scoachy
//
//  Created by Ziyaad Rhyman on 15/11/2017.
//  Copyright © 2017 Ziyaad Rhyman. All rights reserved.
//

import Foundation

struct User: Decodable {
    
    //let UserId: String?
    let ContactsDto: [ContactsDto]?
    //let ErrorCode: Int?
    //let ErrorMessage: String?
}

struct ContactsDto : Decodable {
    
    //let Id : String?
    let UserId : String?
    let FriendUserId : String?
    //let FirstName : String?
    //let LastName : String?
    let Email : String?
    //let PhonePrefix : String?
    let Phone : String?
    let InviteStatus : String?
    let InvitedOn: String?
    //let UserStatus : String?
    //let ErrorCode : Int?
    //let ErrorMessage : String?
}

struct CreateAccount : Decodable {
    
    let ErrorCode : Int?
    let Id : String?
    let Message : String?
    //let Success : Int?
}

struct UserInformation: Decodable {
    
    let UserInfo: UserInfo?
    let AnswerResults : [AnswerResults]?
    //let EncryptedSql : String?
    let LastDbVersion : Int?
    //let Status : [Status]?
}

struct UserInfo : Decodable {
    let Id : String?
    let Username : String?
    let FirstName : String?
    let LastName : String?
    let Gender : String?
    let BirthDate : String?
    let PostalCode : String?
    let Email : String?
    let Phone : String?
    let PhonePrefix : String?
    let Country : String?
    //let EncryptedPassword : String?
    //let Salt : String?
    let Language : String?
    let ReceiveNewsletter : Bool?
    //let RegistrationMode : String?
    //let DeviceToken : String?
    //let Os : String?
    //let OsVersion : String?
    let UserStatus : String?
    let UserType : String?
}

struct Scoacheur: Decodable {
    let Scoachers : Int?
    let Scoached : Int?
}

struct AnswerResults : Decodable {
    let Id : String?
    let FriendUserId : String?
    let AnswerId : String?
    let LastAnsweredDate : String?
    let QuestionId : String?
}

struct Status : Decodable {
    //let Success : Bool?
    //let ErrorCode : Int?
    //let Message : String?
}


