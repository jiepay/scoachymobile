package com.reovo.scoachy_v2;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import flepsik.github.com.progress_ring.ProgressRingView;

public class Act3RelanceDemande extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    Button relancer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3_relance_demande);
    // Fonts
        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        Typeface textOpen = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Regular.ttf");
        Typeface titleDate = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Bold.ttf");

        TextView title = (TextView)findViewById(R.id.titleText);
        TextView text_persones = (TextView) findViewById(R.id.text_persones);
        TextView text_percent = (TextView) findViewById(R.id.text_percent);
        TextView textRelance_int = (TextView)findViewById(R.id.textViewRelance_int);
        TextView textDemande_int =(TextView)findViewById(R.id.textViewDemande_int);
        TextView textNombre_int=(TextView)findViewById(R.id.textViewNombre_int);
        TextView textRelance = (TextView)findViewById(R.id.textViewRelance);
        TextView textDemande =(TextView)findViewById(R.id.textViewDemande);
        TextView textNombre=(TextView)findViewById(R.id.textViewNombre);
        TextView textDate =(TextView)findViewById(R.id.text_date);


    //set Font
        title.setTypeface(face);
        text_persones.setTypeface(face);
        text_percent.setTypeface(face);
        textDemande_int.setTypeface(face);
        textRelance_int.setTypeface(face);
        textNombre_int.setTypeface(face);

        textDemande.setTypeface(textOpen);
        textNombre.setTypeface(textOpen);
        textRelance.setTypeface(textOpen);

        textDate.setTypeface(titleDate);

        text_percent.setTextColor(Color.BLACK);
        text_persones.setTextColor(Color.BLACK);


        ProgressRingView progress_personne = (ProgressRingView) findViewById(R.id.progress_personne);
        setProgressAnim(progress_personne,0.75f);





//button relancer
         relancer= (Button) findViewById(R.id.button_relancer);
        relancer.setTypeface(face);
       relancer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 relancer.setPressed(true);
            }
        });
    }

    public void setProgressAnim(ProgressRingView progress, float progressvalue){
        progress.setAnimated(true);
        progress.setAnimationDuration(5000);
        progress.setProgress(progressvalue);
        return;
    }

}
