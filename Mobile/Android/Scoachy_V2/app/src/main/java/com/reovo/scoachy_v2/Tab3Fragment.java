package com.reovo.scoachy_v2;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.reovo.scoachy_v2.R;

import flepsik.github.com.progress_ring.ProgressRingView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab3Fragment extends Fragment {

    TextView textHeaderTitle;


    public Tab3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View tab3View = inflater.inflate(R.layout.fragment_tab3, container, false);

        Typeface header = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        textHeaderTitle =(TextView)tab3View.findViewById(R.id.headerTitle);
        textHeaderTitle.setTypeface(header);

        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),"fonts/OpenSans-Bold.ttf");


        ProgressRingView progress_contact = (ProgressRingView) tab3View.findViewById(R.id.progress_contact);
        ProgressRingView progress_relance = (ProgressRingView) tab3View.findViewById(R.id.progress_relance);
        ProgressRingView progress_results = (ProgressRingView) tab3View.findViewById(R.id.progress_results);
        ProgressRingView progress_debrief = (ProgressRingView) tab3View.findViewById(R.id.progress_debrief);


        TextView text_contact = (TextView) tab3View.findViewById(R.id.text_contact);
        TextView text_relancer = (TextView) tab3View.findViewById(R.id.text_relancer);
        TextView text_resultats = (TextView) tab3View.findViewById(R.id.text_resultats);
        TextView text_debrief = (TextView) tab3View.findViewById(R.id.text_debrief);

        text_contact.setTypeface(face);
        text_relancer.setTypeface(face);
        text_resultats.setTypeface(face);
        text_debrief.setTypeface(face);



        setProgressAnim(progress_contact,0f);
        setProgressAnim(progress_relance,1.0f);
        setProgressAnim(progress_results,1.0f);
        setProgressAnim(progress_debrief,1.0f);


        progress_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"contact",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),MesContacts.class);
                startActivity(intent);
            }
        });


        progress_relance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"relancer",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act3RelanceDemande.class);
                startActivity(intent);
            }
        });


        progress_results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"resultats",Toast.LENGTH_SHORT).show();
                Intent intent;
                intent = new Intent(getActivity(),Act3ResultAnimal.class);
                startActivity(intent);
            }
        });


        progress_debrief.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"debrief",Toast.LENGTH_SHORT).show();
            }
        });



        return tab3View;
    }


    public void setProgressAnim(ProgressRingView progress, float progressvalue){
        progress.setAnimated(true);
        progress.setAnimationDuration(5000);
        progress.setProgress(progressvalue);
        return;
    }



 
}
