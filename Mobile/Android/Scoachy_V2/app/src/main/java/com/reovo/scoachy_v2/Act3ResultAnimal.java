package com.reovo.scoachy_v2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.reovo.scoachy_v2.Fragment.AnimalAdapter;

import java.util.List;

public class Act3ResultAnimal extends AppCompatActivity implements GestureDetector.OnGestureListener {
    GestureDetector gestureDetector;
    ListView listView;
    String[] values = {"1","2","3"};

    int animalIcons[]= {R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_01};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act3_result_animal);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        listView =(ListView)findViewById(R.id.animals_moi);
        AnimalAdapter animalAdapter =new  AnimalAdapter(this,values,animalIcons);
        listView.setAdapter(animalAdapter);



        gestureDetector = new GestureDetector(Act3ResultAnimal.this, Act3ResultAnimal.this);

    }
    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float X, float Y) {




        if(motionEvent1.getX() - motionEvent2.getX() > 50){

            Toast.makeText(Act3ResultAnimal.this , " Swipe right " , Toast.LENGTH_LONG).show();
            Intent intent;
            intent = new Intent(Act3ResultAnimal.this,Act3ResultAttribut.class);
            startActivity(intent);

            return true;
        }

        if(motionEvent2.getX() - motionEvent1.getX() > 50) {

            Toast.makeText(Act3ResultAnimal.this, " Swipe left ", Toast.LENGTH_LONG).show();





            return true;
        }
        else {

            return true ;
        }
    }

    @Override
    public void onLongPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public void onShowPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onSingleTapUp(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        // TODO Auto-generated method stub

        return gestureDetector.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onDown(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }
}
