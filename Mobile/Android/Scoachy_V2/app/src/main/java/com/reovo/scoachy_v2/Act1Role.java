package com.reovo.scoachy_v2;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Act1Role extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    TextView attributeText, descText,questionText;
    Button rep10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1_role);

        bottomNavigationView =(BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return true;
            }
        });


        Typeface face = Typeface.createFromAsset(this.getAssets(),"fonts/Quicksand-Bold.ttf");
        attributeText = (TextView) findViewById(R.id.txt_Attribute);
        attributeText.setTypeface(face);

        Typeface description = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-SemiboldItalic.ttf");
        descText= (TextView)findViewById(R.id.txt_Description);
        descText.setTypeface(description);

        questionText =(TextView) findViewById(R.id.txt_Question);
        questionText.setTypeface(description);

        Typeface answer = Typeface.createFromAsset(this.getAssets(),"fonts/OpenSans-Bold.ttf");
        rep10= (Button) findViewById(R.id.rep_10);
        rep10.setTypeface(answer);
        rep10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rep10.setPressed(true);
            }
        });
    }
}
