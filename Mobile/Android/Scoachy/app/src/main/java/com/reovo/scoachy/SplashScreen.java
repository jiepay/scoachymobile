package com.reovo.scoachy;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.VideoView;

public class SplashScreen extends AppCompatActivity {
     VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);


//        videoView = (VideoView)findViewById(R.id.videoView);
//        Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash_video_1);
//        videoView .setVideoURI(video);
//        videoView.setOnCompletionListener( new MediaPlayer.OnCompletionListener(){
//
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//
//                if(isFinishing())
//                    return;
//
//                startActivity(new Intent(SplashScreen.this,MainActivity.class));
//                 finish();
//
//
//            }
//        });
//        videoView.start();

        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        myThread.start();
    }
}
