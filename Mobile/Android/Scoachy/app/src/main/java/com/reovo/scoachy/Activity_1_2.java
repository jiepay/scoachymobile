package com.reovo.scoachy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import static android.widget.Toast.makeText;

public class Activity_1_2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView txtQuestion, txtAttribute, txtDescription;
    Button rep1,rep2,rep3,rep4,rep5;
    Date date = new Date();

    ArrayList<QuestionModel> questionAct;
    public static final String PREFSHARE  = "preferenceOthers";
    private DatabaseAccess databaseAccess;
    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1_2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de autres");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_1_2.this, HomeActivity.class);
                // storeAnswer();
                startActivity(Home);
            }
        });

        databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
        databaseAccess.open();
        questionAct = databaseAccess.questionAct();

        dbHelper = new DatabaseHelper(this);


        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);
        rep1 =(Button)findViewById(R.id.rep1);
        rep2 =(Button)findViewById(R.id.rep2);
        rep3 =(Button)findViewById(R.id.rep3);
        rep4 =(Button)findViewById(R.id.rep4);
        rep5 =(Button)findViewById(R.id.rep5);

        //font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
        txtAttribute.setTypeface(typeface);
        rep1.setTypeface(typeface);
        rep2.setTypeface(typeface);
        rep3.setTypeface(typeface);
        rep4.setTypeface(typeface);
        rep5.setTypeface(typeface);

        Typeface question = Typeface.createFromAsset(getAssets(), "fonts/OpenSans_Regular.ttf");
        txtQuestion.setTypeface(question);
        txtDescription.setTypeface(question);




        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        txtAttribute.setText(questionAct.get(data).getAttribute());
        txtQuestion.setText(questionAct.get(data).getDescription());
        txtDescription.setText (questionAct.get(data).getQuest());
        Log.d("Arraylist1", questionAct.get(data).getRatingType());
        Log.d("Arraylist1", questionAct.get(data).getID());
        final String id = questionAct.get(data).getID();

        final String answer_1 = "b7541f37-614d-11e7-a7e0-484d7ee0cd26";
        final String answer_2 = "b75607a3-614d-11e7-a7e0-484d7ee0cd26";
        final String answer_3= "b75618f8-614d-11e7-a7e0-484d7ee0cd26";
        final String answer_4 = "b7562603-614d-11e7-a7e0-484d7ee0cd26";
        final String answer_5 = "b75633c9-614d-11e7-a7e0-484d7ee0cd26";

        date.setTime(System.currentTimeMillis());
        final String dateAnswered = date.toString();
        Log.d("Database", dateAnswered);

        rep1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Log.d("Databas12", id);

                boolean isElement = dbHelper.insertAnswer( "1",answer_1,id, dateAnswered);
                if(isElement == true) {
                  //  makeText(Activity_1_2.this, "Data Inserted 1", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3", "Data Inserted 1");
                }
                else {
                   // makeText(Activity_1_2.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3","Data not Inserted");
                }
                moveNext();

            }
        });
        rep2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                boolean isElement = dbHelper.insertAnswer( "1",answer_2,id, dateAnswered);
                if(isElement == true) {
                    //makeText(Activity_1_2.this, "Data Inserted 2", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3", "Data Inserted 2");
                }
                else {
                  //  makeText(Activity_1_2.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3","Data not Inserted");
                }
                moveNext();

            }
        });
        rep3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                boolean isElement = dbHelper.insertAnswer( "1",answer_3,id, dateAnswered);
                if(isElement == true) {
                  //  makeText(Activity_1_2.this, "Data Inserted 3", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3", "Data Inserted 3");
                }
                else {
                   // makeText(Activity_1_2.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3","Data not Inserted");
                }
                moveNext();

            }
        });
        rep4.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                boolean isElement = dbHelper.insertAnswer( "1",answer_4,id, dateAnswered);
                if(isElement == true) {
                  //  makeText(Activity_1_2.this, "Data Inserted 4", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3", "Data Inserted 4");
                }
                else {
                    //makeText(Activity_1_2.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3","Data not Inserted");
                }
                moveNext();

            }
        });
        rep5.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                boolean isElement = dbHelper.insertAnswer( "1",answer_5,id, dateAnswered);
                if(isElement == true) {
                   // makeText(Activity_1_2.this, "Data Inserted 5", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3", "Data Inserted 5");
                }
                else {
                  //  makeText(Activity_1_2.this, "Data not Inserted", Toast.LENGTH_LONG).show();
                    Log.d("Arraylist3","Data not Inserted");
                }
                moveNext();

            }
        });

    }
    private void storeAnswer() {
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");

        SharedPreferences preferenceQuestion = getSharedPreferences(PREFSHARE, 0);
        SharedPreferences.Editor editor = preferenceQuestion.edit();
        editor.putInt("questionAct1", data);

        editor.commit();
    }

    public void moveNext(){
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("QuestionAct");
        int nextQuestion = data +1;
        int  SizeQuestion =questionAct.size();
        // storeAnswer();


        System.out.println("Heya  " + data);

        if (SizeQuestion == data){
          //  Toast.makeText(this, "You Have Completed the Activity", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_1_2.this,HomeActivity.class);
            intent.putExtra("QuestionAct", nextQuestion);
            startActivity(intent);
        }
        else

            //1
            if (questionAct.get(nextQuestion).getRatingType().equals("competenceChoice")){
            //    Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1_2.this,Activity_1_4.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //2
            else if (questionAct.get(nextQuestion).getRatingType().equals("attributeChoice")){
             //   Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Activity_1_2.this,Activity_1_2.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //3
            else if (questionAct.get(nextQuestion).getRatingType().equals("animalChoice")) {
              //  Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
                //  questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_1.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);
            }
            //4
            else if (questionAct.get(nextQuestion).getRatingType().equals("elementChoice")){
             //   Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_3.class);
                intent.putExtra("QuestionAct",nextQuestion);
                startActivity(intent);
            }
            //5

            else if (questionAct.get(nextQuestion).getRatingType().equals("motivationChoice")){
              //  Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_8.class);
                intent.putExtra("QuestionAct",nextQuestion);
                startActivity(intent);

            }
            //6
            else if (questionAct.get(nextQuestion).getRatingType().equals("seasonChoice")){
              //  Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_5.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }
            //7
            else if (questionAct.get(nextQuestion).getRatingType().equals("roleChoice")){
               // Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_6.class);
                intent.putExtra("QuestionAct",nextQuestion);
                startActivity(intent);

            }
            //8
            else if (questionAct.get(nextQuestion).getRatingType().equals("colorChoice")){
              //  Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
                // questions.remove(0);
                Intent intent = new Intent(Activity_1_2.this, Activity_1_7.class);
                intent.putExtra("QuestionAct", nextQuestion);
                startActivity(intent);

            }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_1_2, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_contact) {
            Intent intentContact = new Intent(this, Activity_1.class);
            startActivity(intentContact);
            //storeAnswer();
            return true;
        }


        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            //storeAnswer();
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
