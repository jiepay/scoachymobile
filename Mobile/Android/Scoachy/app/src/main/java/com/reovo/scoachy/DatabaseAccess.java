package com.reovo.scoachy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.TextView;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Text;


public class DatabaseAccess {

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;


    private DatabaseAccess(Context context){
        this.openHelper = new DatabaseOpenHelper(context);
    }



    public static DatabaseAccess getInstance(Context context){
        if (instance == null){
            instance = new DatabaseAccess(context);
        }
        return instance;
    }



    public void open(){
        this.database = openHelper.getWritableDatabase();

    }

    public void close(){
        if (database != null){
            this.database.close();
        }
    }

//    public void saveAnswer(AnswerModel answer) {
//        database = openHelper.getWritableDatabase();
//       // database.execSQL("INSERT INTO answerresult values('+answer.getAnswerID()+')");
//       ContentValues values =new ContentValues();
//        values.put("id",answer.getAnswerID());
//        database.insert("answerresult",null,values);
//
//       // return 0;
//    }










    public List<String> getQuotes(){

        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM quotes", null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

//    public List<String> getQuestions(){
//
//        List<String> listQuestions = new ArrayList<>();
//        Cursor cursor = database.rawQuery("SELECT description FROM question", null);
//        cursor.moveToFirst();
//        while(!cursor.isAfterLast()){
//            listQuestions.add(cursor.getString(0));
//            cursor.moveToNext();
//        }
//        cursor.close();
//        return listQuestions;
//    }

//activity 2
    public ArrayList<QuestionModel> questions(){

        ArrayList<QuestionModel> questions = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM allquestion ORDER BY orderList ASC ", null);
        QuestionModel questionModel;
        if (cursor.getCount() > 0){
            for(int i = 0; i < cursor.getCount(); i++){
                cursor.moveToNext();
                questionModel = new QuestionModel();
                questionModel.setID(cursor.getString(0));
                questionModel.setAttribute(cursor.getString(18));
                questionModel.setDescription(cursor.getString(6));
                questionModel.setOrder(cursor.getInt(4));
                questionModel.setQuest(cursor.getString(7));
                questionModel.setRatingType(cursor.getString(12));
                questions.add(questionModel);
            }
            cursor.close();
            return questions;
        }

        return questions;
    }




//Activity1
    public ArrayList<QuestionModel> questionAct(){

        ArrayList<QuestionModel> questionAct = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM allquestion_other ORDER BY orderList ASC ", null);
        QuestionModel questionsModel;
        if (cursor.getCount() > 0){
            for(int i = 0; i < cursor.getCount(); i++){
                cursor.moveToNext();
                questionsModel = new QuestionModel();
                questionsModel.setID(cursor.getString(0));
                questionsModel.setAttribute(cursor.getString(18));
                questionsModel.setDescription(cursor.getString(6));
                questionsModel.setOrder(cursor.getInt(4));
                questionsModel.setQuest(cursor.getString(7));
                questionsModel.setRatingType(cursor.getString(12));
                questionAct.add(questionsModel);
            }
            cursor.close();
            return questionAct;
        }

        return questionAct;
    }



//    //AnimalActivity
//    public ArrayList<AnswerModel> animalAct(){
//
//        ArrayList<AnswerModel> animalAct = new ArrayList<>();
//        Cursor cursor = database.rawQuery("SELECT * FROM allquestion_other ORDER BY orderList ASC ", null);
//    AnswerModel answerModel;
//        if (cursor.getCount() > 0){
//            for(int i = 0; i < cursor.getCount(); i++){
//                cursor.moveToNext();
//                answerModel = new AnswerModel();
//                answerModel.setAnswerID(cursor.getString(0));
//                answerModel.setDescription(cursor.getString(2));
//
//                answerModel.setRatingTypeId(cursor.getString(4));
//                animalAct.add(answerModel);
//            }
//            cursor.close();
//            return animalAct;
//        }
//
//        return animalAct;
//    }


//public long insertAnswers(){
//
//    ContentValues values = new ContentValues();
////    values.put("questionId",answer.getID());
////    database.insert("answerresult",null,values);
//
//}




}
