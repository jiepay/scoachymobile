package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cuboid.cuboidcirclebutton.CuboidButton;

import java.util.ArrayList;


public class Grid2_1 extends AppCompatActivity {

    TextView txtQuestion, txtAttribute, txtDescription;
    CuboidButton btnCuboid1, btnCuboid2, btnCuboid3;

    ArrayList<QuestionModel> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid2_1);

        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questions = databaseAccess.questions();

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);

        btnCuboid1 = (CuboidButton) findViewById(R.id.btnCuboid1);
        btnCuboid2 = (CuboidButton) findViewById(R.id.btnCuboid2);
        btnCuboid3 = (CuboidButton) findViewById(R.id.cuboidButton3);

        /*Log.d("Arraylist", questions.get(0).getID());
        Log.d("Arraylist", questions.get(0).getAttribute());
        Log.d("Arraylist", questions.get(0).getDescription());
        Log.d("Arraylist", String.valueOf(questions.get(0).getOrder()));
        Log.d("Arraylist", questions.get(0).getQuest()); */
        Log.d("Arraylist", questions.get(0).getRatingType());

        txtAttribute.setText(questions.get(0).getAttribute());
        txtQuestion.setText(questions.get(0).getQuest());
        txtDescription.setText(questions.get(0).getDescription());
        //Log.d("ArrayList", questions.get(0).getAttribute());

        }

    public void onClick(View view) {

        if (questions.get(1).getRatingType().equals("textChoice")){
            Toast.makeText(Grid2_1.this, "TextChoice", Toast.LENGTH_SHORT).show();
          //  questions.remove(0);
            Intent intent = new Intent(Grid2_1.this, Grid2_2.class);
            startActivity(intent);
        }

        else if (questions.get(1).getRatingType().equals("textChoiceInfo")) {
            Toast.makeText(Grid2_1.this, "textChoiceInfo", Toast.LENGTH_SHORT).show();
          //  questions.remove(0);
            Intent intent = new Intent(Grid2_1.this, Grid2_3.class);
            startActivity(intent);
        }


        else if (questions.get(1).getRatingType().equals("multipleChoice")){
            Toast.makeText(Grid2_1.this, "multipleChoice", Toast.LENGTH_SHORT).show();
           // questions.remove(0);
            Intent intent = new Intent(this, Grid2_4.class);
            startActivity(intent);
        }

        else if (questions.get(1).getRatingType().equals("circularTextChoice")){
            Toast.makeText(Grid2_1.this, "circularTextChoice", Toast.LENGTH_SHORT).show();
           // questions.remove(0);
            Intent intent = new Intent(this, Grid2_1.class);
            startActivity(intent);

        }

    }
}
