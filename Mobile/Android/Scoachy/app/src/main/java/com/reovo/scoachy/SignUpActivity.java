package com.reovo.scoachy;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.util.Arrays;


public class SignUpActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener  {

    private SignInButton google_login_button;
    private GoogleApiClient googleApiClient;
    public static final int SIGN_IN_CODE = 777;

    LoginButton login_button;
    CallbackManager callbackManager;

    private ImageView imgProfile;
    private TextView txtDetails;
    DatabaseHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        computePakageHash();

        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_up);

        // Facebook Sign In

        initializeControls();
        loginWithFacebook();

        // Google Sign In
        // Configure sign-in to request the user's ID, email address, and basic profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the options specified by gso.
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        google_login_button = (SignInButton) findViewById(R.id.google_login_button);
        google_login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, 777);
            }
        });

    }


    private void computePakageHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.reovo.scoachy",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            Log.e("TAG",e.getMessage());
        }
    }




    // Gmail

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()){
            Toast.makeText(SignUpActivity.this, "Google Login Success", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, GoogleTestActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(SignUpActivity.this, "Google Login Failed", Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }



    // Facebook

    private void initializeControls(){
        // Facebook Initialize
        callbackManager = CallbackManager.Factory.create();
        login_button = (LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions(Arrays.asList("email"));

        // LinkedIn Initialize
        ImageView imgLogin = (ImageView)findViewById(R.id.imgLogin);
        imgLogin.setOnClickListener(this);
        //Button btnLogout = (Button)findViewById(R.id.btnLogout);


    }

    private void loginWithFacebook(){
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(SignUpActivity.this, "Facebook Login Success", Toast.LENGTH_SHORT).show();
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e("Access Token:", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("login results", response.toString());
                        Bundle bFacebookdata = getFacebookData(object);

                    }
                });

                Bundle parameter = new Bundle();
                parameter.putString("fields", "id, first_name, last_name, email, gender, birthday, location");
                request.setParameters(parameter);
                request.executeAsync();

                Intent homeActivity = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(homeActivity);
            }

            @Override
            public void onCancel() {
                Toast.makeText(SignUpActivity.this, "Facebook Login Cancel", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(SignUpActivity.this, "Facebook Login Error", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private Bundle getFacebookData(JSONObject object) {
        Bundle bundle = new Bundle();
        try{
            if (object.has("email")){
                bundle.putString("email", object.getString("email"));
                Log.e("email", object.getString("email"));
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        return null;
    }

    // LinkedIn

    private void loginWithLinkedIn(){
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Authentication was successful.  You can now do
                // other calls with the SDK.
                Toast.makeText(SignUpActivity.this, "LinkedIn Login Success", Toast.LENGTH_SHORT).show();
                fetchPersonalInfo();
                Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(intent);

            }

            @Override
            public void onAuthError(LIAuthError error) {
                // Handle authentication errors
                Toast.makeText(SignUpActivity.this, "LinkedIn Login Failed" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        }, true);
    }


    // Build the list of member permissions our LinkedIn session requires
    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE, Scope.R_EMAILADDRESS);
    }




    private void fetchPersonalInfo(){
        String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,public-profile-url,picture-url,email-address,picture-urls::(original))";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                try {
                    JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                    String firstName = jsonObject.getString("firstName");
                    String lastName = jsonObject.getString("lastName");
                    String pictureUrl = jsonObject.getString("pictureUrl");
                    String emailAddress = jsonObject.getString("emailAddress");

                    Log.e("email", jsonObject.getString("emailAddress"));
                    Log.e("firstName", jsonObject.getString("firstName"));
                    Log.e("lastName", jsonObject.getString("lastName"));
                    Log.e("pictureUrl", jsonObject.getString("pictureUrl"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                // Error making GET request!
                Log.e("NIKHIL",liApiError.getMessage());
            }
        });
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    public void onClick1(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
