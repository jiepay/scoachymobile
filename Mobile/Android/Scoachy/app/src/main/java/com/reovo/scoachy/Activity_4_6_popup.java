package com.reovo.scoachy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class Activity_4_6_popup extends AppCompatActivity {
    ImageButton close;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4_6_popup);

        close=(ImageButton)findViewById(R.id.btn_close);
        close.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent valider = new Intent(Activity_4_6_popup.this, Activity_4_6.class);
                startActivity(valider);


            }


        });
    }
}
