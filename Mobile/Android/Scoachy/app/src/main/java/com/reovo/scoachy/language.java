package com.reovo.scoachy;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.linkedin.platform.LISessionManager;

import java.util.Locale;

public class language extends AppCompatActivity {


    Spinner spinnerCtrl;
    Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        spinnerCtrl = (Spinner)findViewById(R.id.spinner1);
        String[] items = new String[]{"Select Language", "English", "French", "German", "Portuguese", "Spanish", "Italian", "Chinese"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinnerCtrl.setAdapter((adapter));
        spinnerCtrl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position ==0){
                    // Do nothing ..
                }

                if(position ==1){
                    Toast.makeText(parent.getContext(), "You have selected English", Toast.LENGTH_SHORT).show();
                    setLocale("en");
                }

                else if (position ==2){
                    Toast.makeText(parent.getContext(), "You have selected French", Toast.LENGTH_SHORT).show();
                    setLocale("fr");
                }

                else if (position==3){
                    Toast.makeText(parent.getContext(), "You have selected German", Toast.LENGTH_SHORT).show();
                    setLocale("de");
                }

                else if (position==4){
                    Toast.makeText(parent.getContext(), "You have selected Portuguese", Toast.LENGTH_SHORT).show();
                    setLocale("pt");
                }

                else if (position==5){
                    Toast.makeText(parent.getContext(), "You have selected Spanish", Toast.LENGTH_SHORT).show();
                    setLocale("es");
                }

                else if (position==6){
                    Toast.makeText(parent.getContext(), "You have selected Italian", Toast.LENGTH_SHORT).show();
                    setLocale("it");
                }

                else if (position==7){
                    Toast.makeText(parent.getContext(), "You have selected Chinese", Toast.LENGTH_SHORT).show();
                    setLocale("zh");
                }

            }



            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_account:
                Toast.makeText(language.this, "Account", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_language:
                Toast.makeText(language.this, "Language", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, language.class);
                startActivity(intent);
                return true;
//            case R.id.item_logout:
//                //Facebook Logout
//                AccessToken.setCurrentAccessToken(null);
//                Profile.setCurrentProfile(null);
//                //Gmail Logout
//                //LinkedIn Logout
//                LISessionManager.getInstance(getApplicationContext()).clearSession();
//                //Navigate to Login Screen
//                intent = new Intent(this, MainActivity.class);
//                startActivity(intent);
//
//                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    public void setLocale(String lang){

        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, language.class);
        startActivity(refresh);

    }

    public void home(View view) {

        Intent intent = new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
}
