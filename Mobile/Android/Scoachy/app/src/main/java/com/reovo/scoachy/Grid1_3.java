package com.reovo.scoachy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Grid1_3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid1_3);
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Grid1_4.class);
        startActivity(intent);
    }
}
