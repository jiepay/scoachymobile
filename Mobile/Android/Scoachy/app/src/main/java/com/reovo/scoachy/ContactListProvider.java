package com.reovo.scoachy;

/**
 * Created by DummyReovo on 11/05/2017.
 */

public class ContactListProvider {
    private int img_contact;
    private String contact;
    private String number;


    public int getImg_contact() {
        return img_contact;
    }

    public ContactListProvider(int img_contact, String contact, String number){
        this.setImg_contact(img_contact);
        this.setContact(contact);
        this.setNumber(number);
    }

    public void setImg_contact(int img_contact) {
        this.img_contact = img_contact;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

