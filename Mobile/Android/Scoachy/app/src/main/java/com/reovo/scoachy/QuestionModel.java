package com.reovo.scoachy;

public class QuestionModel {

    private String ID, description, attribute, quest, ratingType ;
    private int order;

    public String getID(){
        return ID;
    }

    public void setID(String ID){
        this.ID = ID;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getAttribute(){
        return attribute;
    }

    public void setAttribute(String attribute){
        this.attribute = attribute;
    }

    public String getQuest(){
        return quest;
    }

    public void setQuest(String quest){
        this.quest = quest;
    }

    public int getOrder(){
        return order;
    }

    public void setOrder(int order){
        this.order = order;
    }

    public String getRatingType(){
        return ratingType;
    }

    public void setRatingType(String ratingType){
        this.ratingType = ratingType;
    }


}
