package com.reovo.scoachy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class Activity_2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    GridView gv;
    String animalName[]= {"Corbeau","Aigle","Araignée","Carpe","Castor","Chat","Cheval","Chien","Chouette","Coq","Cygne","Dauphin","Éléphant","Fouine","Gazelle","Lézard","Libellule","Lion/Lionne","Loup/Louve","Marmotte","Moustique","Mouton","Mule","Ours","Paon","Papillon","Perroquet","Renard","Requin","Serpent","Singe","Souris","Tigre/Tigresse","Tortue","Caméléon"};

    int animalIcons[]= {R.drawable.animaux_scoachy_orange_01,R.drawable.animaux_scoachy_orange_02,R.drawable.animaux_scoachy_orange_03,R.drawable.animaux_scoachy_orange_04,R.drawable.animaux_scoachy_orange_05,R.drawable.animaux_scoachy_orange_06,R.drawable.animaux_scoachy_orange_07,R.drawable.animaux_scoachy_orange_08,R.drawable.animaux_scoachy_orange_09,R.drawable.animaux_scoachy_orange_10,R.drawable.animaux_scoachy_orange_11,R.drawable.animaux_scoachy_orange_12,R.drawable.animaux_scoachy_orange_13,R.drawable.animaux_scoachy_orange_14,R.drawable.animaux_scoachy_orange_15,R.drawable.animaux_scoachy_orange_16,R.drawable.animaux_scoachy_orange_17,R.drawable.animaux_scoachy_orange_18,R.drawable.animaux_scoachy_orange_19,R.drawable.animaux_scoachy_orange_20,R.drawable.animaux_scoachy_orange_21,R.drawable.animaux_scoachy_orange_22,R.drawable.animaux_scoachy_orange_23,R.drawable.animaux_scoachy_orange_24,R.drawable.animaux_scoachy_orange_25,R.drawable.animaux_scoachy_orange_26,R.drawable.animaux_scoachy_orange_27,R.drawable.animaux_scoachy_orange_28,R.drawable.animaux_scoachy_orange_29,R.drawable.animaux_scoachy_orange_30,R.drawable.animaux_scoachy_orange_31,R.drawable.animaux_scoachy_orange_32,R.drawable.animaux_scoachy_orange_33,R.drawable.animaux_scoachy_orange_34,R.drawable.animaux_scoachy_orange_35};
    TextView txtQuestion, txtAttribute, txtDescription;
    ArrayList<QuestionModel> questions;
    Date date = new Date();
    public static final String PREFS  = "preferenceQuestion";
    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Ce que je pense de moi");



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View logoView =navigationView.getHeaderView(0);
        ImageView logoHeader = (ImageView)logoView.findViewById(R.id.logoNav);
        logoHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent Home = new Intent(Activity_2.this, HomeActivity.class);
                storeAnswer();
                startActivity(Home);
            }
        });



        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        questions = databaseAccess.questions();
        dbHelper = new DatabaseHelper(this);

        txtQuestion = (TextView) findViewById(R.id.txt_Question);
        txtDescription = (TextView) findViewById(R.id.txt_Description);
        txtAttribute = (TextView) findViewById(R.id.txt_Attribute);

        //font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
        txtAttribute.setTypeface(typeface);

        Typeface question = Typeface.createFromAsset(getAssets(), "fonts/OpenSans_Regular.ttf");
        txtQuestion.setTypeface(question);



        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");
        txtAttribute.setText(questions.get(data).getAttribute());
        txtQuestion.setText(questions.get(data).getDescription());
        txtDescription.setText(questions.get(data).getQuest());
        Log.d("Arraylist3", questions.get(data).getRatingType());

        gv = (GridView)findViewById(R.id.animalOrangeGridview);
        Typeface grid = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Medium.ttf");

        GridAnimalAdapter animalAdapter = new GridAnimalAdapter(Activity_2.this,animalIcons,animalName,grid);

        gv.setAdapter(animalAdapter);
        date.setTime(System.currentTimeMillis());
        final String dateAnswered = date.toString();
        Log.d("Database", dateAnswered);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(Activity_1_1.this,"Clicked"+animalName[position],Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(Activity_2.this,Activity_2_1.class);
//                startActivity(intent);
   //             Toast.makeText(Activity_2.this, "" + position, Toast.LENGTH_SHORT).show();

                moveNext();

            }
        });
    }


    private void storeAnswer() {
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");

        SharedPreferences preferenceQuestion = getSharedPreferences(PREFS, 0);
        SharedPreferences.Editor editor = preferenceQuestion.edit();
        editor.putInt("QuestionMessage", data);

        editor.commit();
    }
    public void moveNext(){
        Bundle bundle = getIntent().getExtras();
        int data = bundle.getInt("Question");
        int nextQuestion = data +1;
        storeAnswer();



        //1
        if (questions.get(nextQuestion).getRatingType().equals("competenceChoice")){
         //   Toast.makeText(this, "competenceChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_2.this,Activity_2_3.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }
        //2
        else
            if (questions.get(nextQuestion).getRatingType().equals("attributeChoice")){
          //  Toast.makeText(this, "attributeChoice", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Activity_2.this,Activity_2_1.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }
        //3
        else if (questions.get(nextQuestion).getRatingType().equals("animalChoice")) {
          //  Toast.makeText(this, "animalChoice", Toast.LENGTH_SHORT).show();
            //  questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);
        }
        //4
        else if (questions.get(nextQuestion).getRatingType().equals("elementChoice")){
         //   Toast.makeText(this, "elementChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2_2.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);
        }
        //5

        else if (questions.get(nextQuestion).getRatingType().equals("motivationChoice")){
          //  Toast.makeText(this, "motivationChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2_7.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }
        //6
        else if (questions.get(nextQuestion).getRatingType().equals("seasonChoice")){
          //  Toast.makeText(this, "seasonChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2_4.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }
        //7
        else if (questions.get(nextQuestion).getRatingType().equals("roleChoice")){
          //  Toast.makeText(this, "roleChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2_5.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }
        //8
        else if (questions.get(nextQuestion).getRatingType().equals("colorChoice")){
         //   Toast.makeText(this, "colorChoice", Toast.LENGTH_SHORT).show();
            // questions.remove(0);
            Intent intent = new Intent(Activity_2.this, Activity_2_6.class);
            intent.putExtra("Question", nextQuestion);
            startActivity(intent);

        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.activity_2, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            Intent intentAccount = new Intent(this, Activity_UserAccount.class);
            startActivity(intentAccount);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
