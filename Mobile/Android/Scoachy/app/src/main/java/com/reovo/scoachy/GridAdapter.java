package com.reovo.scoachy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by DummyReovo on 30/05/2017.
 */

public class GridAdapter extends BaseAdapter {

    private Context context;
    private int images[];
    private String color_ids[];
    private LayoutInflater inflater;

    public GridAdapter(Context context, int images[], String color_ids[]) {
        this.context = context;
        this.images = images;
        this.color_ids = color_ids;
    }


    @Override
    public int getCount() {
        return color_ids.length;
    }

    @Override
    public Object getItem(int position) {
        return color_ids[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View gridView = convertView;
        if(convertView ==null){
            inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.custom_grid,null);
        }
        ImageView image = (ImageView) gridView.findViewById(R.id.image_color);
        TextView color_id = (TextView) gridView.findViewById(R.id.colorId);

        image.setImageResource(images[position]);
        color_id.setText(color_ids[position]);


        return gridView;
    }
}
